﻿using UnityEngine;
using System.Collections;

public class Spaceship : MonoBehaviour {

    DisplayElements displayElements;

    public int TotalMetal = 0;
    public int TotalGas = 0;
    public int TotalCrystal = 0;

    public bool upgImproveSpeed = false;
    public bool upgImproveEnergyEfficient = false;
    public bool upgImproveMaxEnergyHealth1 = false;
    public bool upgImproveMaxEnergyHealth2 = false;
    public bool upgInventory1 = false;
    public bool upgInventory2 = false;

    private PlayerController playerController;


    void Start()
    {
        displayElements = GameObject.FindObjectOfType<DisplayElements>();
        playerController = GameObject.FindObjectOfType<PlayerController>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            RechargeVehicle();
        }

        if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            Application.LoadLevel("Scene");
        }
    }

    public void RechargeVehicle()
    {
        StartCoroutine(CoRechargeVehicle());
        
    }

    private IEnumerator CoRechargeVehicle()
    {
        yield return StartCoroutine(displayElements.CoStartAction("RECHARGING AND UNLOADING", 5f));

        //Add to total
        TotalMetal += displayElements.Metal;
        TotalGas += displayElements.Gas;
        TotalCrystal += displayElements.Crystal;

        //Reset vehicle stuff
        displayElements.Metal = displayElements.Gas = displayElements.Crystal = 0;
        displayElements.SetCargo(0);
        displayElements.SetHealth(displayElements.MaxHealth);
        displayElements.SetEnergy(displayElements.MaxEnergy);

        //TODO rotate vehicle
        CheckForUpgrade();
    }

    public void CheckForUpgrade()
    {
        string totalUpdates = "";

        if (TotalCrystal > 20 && !upgImproveMaxEnergyHealth2)
        {
            upgImproveMaxEnergyHealth2 = true;
            totalUpdates += "SPEED UPGRADED\n";
            displayElements.MaxHealth += 3;
            displayElements.MaxEnergy += 3;
            displayElements.SetHealth(displayElements.MaxHealth);
            displayElements.SetEnergy(displayElements.MaxEnergy);
        }
        else if (TotalCrystal > 10 && !upgImproveMaxEnergyHealth1)
        {
            upgImproveMaxEnergyHealth1 = true;
            totalUpdates += "SPEED UPGRADED\n";
            displayElements.MaxHealth += 3;
            displayElements.MaxEnergy += 3;
            displayElements.SetHealth(displayElements.MaxHealth);
            displayElements.SetEnergy(displayElements.MaxEnergy);
        }

        if (TotalGas > 20 && !upgImproveSpeed)
        {
            upgImproveSpeed = true;
            totalUpdates += "SPEED UPGRADED\n";
            playerController.moveSpeed += 4;
            
        }
        else if (TotalGas > 10 && !upgImproveEnergyEfficient)
        {
            upgImproveEnergyEfficient = true;
            totalUpdates += "ENERGY USAGE UPGRADED\n";
            playerController.triggerMoveEnergyDown += 200;
        }

        if (TotalMetal > 20 && !upgInventory2)
        {
            upgInventory2 = true;
            totalUpdates += "CARGO UPGRADED\n";
            displayElements.MaxCargo += 3;
            displayElements.SetCargo(0);
        }
        else if (TotalMetal > 10 && !upgInventory1)
        {
            upgInventory1 = true;
            totalUpdates += "CARGO UPGRADED\n";
            displayElements.MaxCargo += 3;
            displayElements.SetCargo(0);
        }
        displayElements.ShowNotification(totalUpdates);
    }



}
