﻿using UnityEngine;
using System.Collections;
using System;

public class SpaceshipEnterPoint : MonoBehaviour {

    PlayerController playerController;
    Spaceship ship;
	Quaternion rot;

    void Start()
    {
        ship = GameObject.FindObjectOfType<Spaceship>();
        playerController = GameObject.FindObjectOfType<PlayerController>();
		rot = playerController.transform.rotation;
    }

    void OnTriggerEnter(Collider collider)
    {
		playerController.enabled = false;
		playerController.rigidbody.isKinematic = true;
		Debug.Log("YOOO");
        ship.RechargeVehicle();
        CameraFade.StartAlphaFade(Color.black, false, 2f, 4f, () =>
        {
            ResetObject();
        });
    }

    void ResetObject()
    {
		var go = GameObject.Find("StartingPosition");
        CameraFade.StartAlphaFade(Color.black, true, 1f);
		playerController.transform.position = go.transform.position;
		playerController.enabled = true;
		playerController.rigidbody.isKinematic = false;
		playerController.transform.rotation = rot;
    }
}
