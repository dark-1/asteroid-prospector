﻿using UnityEngine;
using System.Collections;

public class PlayerCamera : MonoBehaviour {

	public bool limitView = true;
	public float mouseSensitivity = 5;
	public bool visorOn = false;
	public Camera weaponCamera;

//	public Camera visorCam;
	private PlayerHUD hud;

	private Transform target;

	// Use this for initialization
	void Start () {
		hud = GetComponent<PlayerHUD>();
		Screen.showCursor = false;
		Screen.lockCursor = false;
	}
	
	// Update is called once per frame
	void Update () {
		Quaternion prevRotation = transform.rotation;
		transform.Rotate(-Input.GetAxis("Mouse Y") * mouseSensitivity, 0, 0, Space.Self);

		//Reset rotation if too high
//		if(transform.parent.parent != null && Vector3.Angle(transform.forward, transform.parent.up) < 10f || Vector3.Angle(-transform.forward, transform.parent.up) < 10f ){
//			transform.rotation = prevRotation;
//		}

		transform.parent.Rotate(0, Input.GetAxis("Mouse X") * mouseSensitivity, 0, Space.Self);


		//Visor
		if(Input.GetButtonDown("Visor")){
			visorOn = !visorOn;
			if(visorOn){
				SetVisorScripts(true);
			}
			else{
				SetVisorScripts(false);
			}
		}

//		if(Input.GetButtonDown("Fire1") && target != null){
//			transform.parent.GetComponent<FauxGravityBody>().attractor = target.GetComponent<FauxGravityAttractor>();
//		}
	}

	void FixedUpdate(){
		Ray ray = camera.ScreenPointToRay(new Vector2(Screen.width/2, Screen.height/2));
		RaycastHit hitInfo;
		bool didHit = Physics.Raycast(ray, out hitInfo);
		if(didHit && LayerMask.NameToLayer("Asteroids") == hitInfo.collider.gameObject.layer){
			hud.asteroidHover = true;
		}
		else{
			hud.asteroidHover = false;
		}
	}

	void SetVisorScripts(bool enabled){
//		GetComponent<SunShafts>().enabled = enabled;
//		GetComponent<FastBloom>().enabled = enabled;
		GetComponent<GrayscaleEffect>().enabled = enabled;
//		GetComponent<Vignetting>().enabled = enabled;
		GetComponent<VisorGUI>().enabled = enabled;
		weaponCamera.GetComponent<GrayscaleEffect>().enabled = enabled;

        NoiseAndGrain noise = GameObject.FindObjectOfType<NoiseAndGrain>();
		if(enabled){
			noise.generalIntensity = 4.33f;
			noise.tiling = new Vector3(32, 64, 16);
		}
		else{
			noise.generalIntensity = 1f;
			noise.tiling = new Vector3(64, 64, 64);
		}

//		visorCam.enabled = enabled;
	}
}
