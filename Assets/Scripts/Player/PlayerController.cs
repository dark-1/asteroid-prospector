﻿    using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float moveSpeed = 15;
	public float jumpPower;
	public float boostPower;
	public PlayerCamera mainCamera;
	private Vector3 moveDirection;
	FauxGravityAttractor parentAttractor;
    DisplayElements displayElements;
    
    public float triggerMoveEnergyDown = 500;
    float currentTriggerEnergyDown = 0;
    
	private FauxGravityBody body;
	private int asteroidLayer;

	void Start() {
		body = GetComponent<FauxGravityBody>();
		asteroidLayer = LayerMask.NameToLayer("Asteroids");
	}

    void Awake()
    {
        displayElements = GameObject.FindObjectOfType<DisplayElements>();
    }

    void Update()
    {
        moveDirection = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;
    }

	void FixedUpdate() {
        if (displayElements.Energy > 0)
        {
            rigidbody.MovePosition(rigidbody.position + transform.TransformDirection(moveDirection) * moveSpeed * Time.deltaTime);
            currentTriggerEnergyDown += Mathf.Abs(moveDirection.x) + Mathf.Abs(moveDirection.y) + Mathf.Abs(moveDirection.z);
            if (currentTriggerEnergyDown >= triggerMoveEnergyDown)
            {
                currentTriggerEnergyDown = 0;
                displayElements.SetEnergy(--displayElements.Energy);
            }
        }
		
		
		if(Input.GetAxis("Jump")>0){
			rigidbody.AddForce(mainCamera.transform.up * jumpPower * Input.GetAxis("Jump"));
		}
		if(Input.GetAxis("Boost")>0){
			rigidbody.AddForce(mainCamera.transform.forward * boostPower * Input.GetAxis("Boost"));
		}
		if(Input.GetButton("Brake")){
			rigidbody.AddForce(-rigidbody.velocity);
		}


		//Detect if near asteroid
		Collider[] colliders = Physics.OverlapSphere(transform.position, 15f);
		int asteroidLayer = LayerMask.NameToLayer("Asteroids");
		int asteroidCount = 0;
		
		for(int i=0; i<colliders.Length; ++i){
			if(colliders[i].gameObject.layer == asteroidLayer){
				asteroidCount++;
				body.attractor = colliders[i].GetComponent<FauxGravityAttractor>();
			}
		}

		if(asteroidCount == 0){
			transform.parent = null;
			body.attractor = null;
		}
	}

	void OnCollisionEnter(Collision c) {
		DetectSetParent(c);

		//Slow the fuck down.
		rigidbody.velocity = Vector3.Min(rigidbody.velocity, new Vector3(.1f, .1f, 0));
	}
	
	void OnCollisionStay(Collision c) {
		DetectSetParent(c);
	}

	void DetectSetParent(Collision c){
		FauxGravityAttractor attractor = c.collider.GetComponent<FauxGravityAttractor>();
		if(attractor != null){
			transform.parent = attractor.transform;
//			parentAttractor = attractor;
//			transform.parent = parentAttractor.transform;
		}
	}
}
