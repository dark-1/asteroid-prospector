﻿using UnityEngine;
using System.Collections;

public class VisorGUI : MonoBehaviour {
	private Renderer[] renderers;
	private MiningResource[] mineResources;
	private Spaceship spaceship;
	private int hotspotlayer;

	public Texture2D blueTex;

	public Rect  blueRect;
	public Rect  redRect;
	public Rect  greenRect;
	public Rect  yellowRect;

	// Use this for initialization
	void Start () {
		mineResources = (MiningResource[])(FindObjectsOfType(typeof(MiningResource)));
		spaceship = GameObject.FindObjectOfType<Spaceship>();
		hotspotlayer = LayerMask.NameToLayer("Hotspots");
		//You probably want to cache this at the beginning.
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnGUI(){
		mineResources = (MiningResource[])(FindObjectsOfType(typeof(MiningResource)));
		for(int i=0; i<mineResources.Length; ++i) {
			if(Vector3.Distance(mineResources[i].transform.position, transform.position) < 200f && mineResources[i].renderer != null && mineResources[i].renderer.isVisible && mineResources[i].renderer.gameObject.layer == hotspotlayer) {
//				RaycastHit hitInfo;
//				bool hit = Physics.Raycast(transform.position, r.transform.position, out hitInfo);
//				if(hit && hitInfo.collider.gameObject == r.gameObject){

				Vector3 screenPoint = camera.WorldToScreenPoint(mineResources[i].transform.position);
				Rect texRect;
				switch(mineResources[i].ResourceType){
				case MiningResource.MiningResourceType.DrillResource:
					texRect = redRect;
					break;
				case MiningResource.MiningResourceType.LaserResource:
					texRect = blueRect;
					break;
				default:
					texRect = greenRect;
					break;
				}
				GUI.DrawTextureWithTexCoords(new Rect(screenPoint.x - 16f, (Screen.height -screenPoint.y)-40f, 32f, 32f), blueTex, texRect);
//				}
			}
		}

		if (spaceship.renderer.isVisible)
		{
		Vector3 point = camera.WorldToScreenPoint(spaceship.transform.position);
		GUI.DrawTextureWithTexCoords(new Rect(point.x - 16f, (Screen.height -point.y)-40f, 32f, 32f), blueTex, yellowRect);
		}

	}
}
