﻿using UnityEngine;
using System.Collections;

public class MiningResource : MonoBehaviour {

    private static DisplayElements guiElements;

    public enum MiningResourceType
    {
        DrillResource,
        LaserResource,
        RemoteDrillResource
    }

    public int CargoLoad;

    [HideInInspector]
    public MiningResourceType ResourceType;
    public float timeNeeded;

    void Awake()
    {
        guiElements = GameObject.FindObjectOfType<DisplayElements>();
    }

    public void OnMinedResource()
    {
        DisplayElements.CargoType cargoType = DisplayElements.CargoType.Metal;

        switch (ResourceType)
        {
            case MiningResourceType.DrillResource:
                cargoType = DisplayElements.CargoType.Metal;
                break;
            case MiningResourceType.LaserResource:
                cargoType = DisplayElements.CargoType.Crystal;
                break;
            case MiningResourceType.RemoteDrillResource:
                cargoType = DisplayElements.CargoType.Gas;
                break;
        }

        //If cargo is not overloaded
        if (guiElements.SetCargo(guiElements.Cargo + CargoLoad, cargoType))
        {
            
            Destroy(gameObject);
        }
    }
	
}
