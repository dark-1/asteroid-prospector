﻿using UnityEngine;
using System.Collections;

public class RemoteDrillResource : MiningResource
{

    // Use this for initialization
    void Start()
    {
        this.ResourceType = MiningResourceType.RemoteDrillResource;
    }

}
