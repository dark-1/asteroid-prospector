﻿using UnityEngine;
using System.Collections;

public class RemoteDrillPickerWeapon : Weapon
{
    public override void FireWeapon()
    {
        var cam = Camera.main.transform;
        RaycastHit rayInfo;
        if (Physics.Raycast(cam.position, cam.forward, out rayInfo, 3f))
        {
            var resource = rayInfo.transform.gameObject.GetComponent<MiningResource>();
            if (resource != null)
            {
                if (CanMineResource == resource.ResourceType)
                {
                    playerWeaponController.IsControlEnabled = false;
                    //TODO Plant drill

                    //MineResource(resource);
                    //Debug.Log("Mined: " + resource);
                }
            }
        }
    }


    public override void OnMiningEnd()
    {
        base.OnMiningEnd();
        playerWeaponController.IsControlEnabled = true;
    }


}
