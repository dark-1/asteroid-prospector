﻿using UnityEngine;
using System.Collections;

public class RemoteDrillWeapon : Weapon
{
    public GameObject bullet;
    public override void FireWeapon()
    {
        var cam = Camera.main.transform;
        RaycastHit rayInfo;
        if (Physics.Raycast(cam.position, cam.forward, out rayInfo, 3f))
        {
            var resource = rayInfo.transform.gameObject.GetComponent<MiningResource>();
            if (resource != null)
            {
                if (CanMineResource == resource.ResourceType)
                {
                    playerWeaponController.IsControlEnabled = false;
                    //TODO Plant drill
                    PlantDrill(rayInfo.collider.transform);
                    MineResource(resource);
                    //Debug.Log("Mined: " + resource);
                }
            }
        }
    }

    public override void MineResource(MiningResource resource)
    {
        base.MineResource(resource);
    }

    void PlantDrill(Transform transform)
    {
        GameObject g =Instantiate(bullet, transform.position + transform.up/2, playerWeaponController.transform.rotation) as GameObject;
		g.transform.parent = transform.parent;
    }


}
