﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour
{

    public int damageWhenWorking;
    public int energyWhenWorking;
    public string actionName;
    protected DisplayElements guiElements;
    protected PlayerWeaponController playerWeaponController;

    public MiningResource.MiningResourceType CanMineResource;

    void Awake()
    {
        guiElements = GameObject.FindObjectOfType<DisplayElements>();
        playerWeaponController = GameObject.FindObjectOfType<PlayerWeaponController>();
    }

    public virtual void FireWeapon()
    {
        var cam = Camera.main.transform;
        RaycastHit rayInfo;
        if (Physics.Raycast(cam.position, cam.forward, out rayInfo, 3f))
        {
            var resource = rayInfo.transform.gameObject.GetComponent<MiningResource>();
            if (resource != null)
            {
                if (CanMineResource == resource.ResourceType)
                {
                    if (guiElements.SetHealth(guiElements.Health - damageWhenWorking))
                    {
                        playerWeaponController.IsControlEnabled = false;
                        MineResource(resource);
                        Debug.Log("Mined: " + resource);
                    }
                }
            }
        }
    }

    public virtual void MineResource(MiningResource resource)
    {
        StartCoroutine(CoMineResource(resource));
    }

    private IEnumerator CoMineResource(MiningResource resource)
    {
        yield return StartCoroutine(guiElements.CoStartAction(actionName, resource.timeNeeded));
        resource.OnMinedResource();
        OnMiningEnd();
    }

    public virtual void Initialize()
    {
        guiElements.SetWeapon(name.ToUpper());
    }

    public virtual void OnMiningEnd()
    {

    }

}
