﻿using UnityEngine;
using System.Collections;

public class PlayerWeaponController : MonoBehaviour
{

    public enum WeaponType
    {
        Drill,
        Laser,
        RemoteDrill
    }

    public Weapon currentWeapon;
    public Weapon[] weapons;

    private PlayerController controller;
    private PlayerCamera playerCam;

    bool _isControlEnabled = true;
	bool clicking;
    public bool IsControlEnabled
    {
        get
        {
            return _isControlEnabled;
        }

        set
        {
            _isControlEnabled = value;

            if (_isControlEnabled)
            {
                controller.enabled = true;
                playerCam.enabled = true;

            }
            else
            {
                controller.enabled = false;
                playerCam.enabled = false;
            }

        }
    }

    void Awake()
    {
        controller = GameObject.FindObjectOfType<PlayerController>();
        playerCam = GameObject.FindObjectOfType<PlayerCamera>();
    }

    void FixedUpdate()
    {
        if (!IsControlEnabled)
            return;

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            ChangeWeapon(WeaponType.Drill);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ChangeWeapon(WeaponType.Laser);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            ChangeWeapon(WeaponType.RemoteDrill);
        }

        if (Input.GetButtonDown("Fire1") && transform.parent != null)
        {
            currentWeapon.FireWeapon();
        }

		if (Input.GetAxis("Mouse ScrollWheel") > 0 || Input.GetButtonDown("NextWeapon"))
        {
			clicking=true;
            NextWeapon();
        }
		else if (Input.GetAxis("Mouse ScrollWheel") < 0 || Input.GetButtonDown("PrevWeapon"))
        {
			clicking=true;
            PreviousWeapon();
        }
    }

    public void ChangeWeapon(WeaponType type)
    {
//		for(int i=0; i<weapons.Length; ++i){
//			weapons[i].gameObject.SetActive(false);
//		}
        switch (type)
        {
            case WeaponType.Drill:
                currentWeapon = weapons[0];
                currentWeapon.CanMineResource = MiningResource.MiningResourceType.DrillResource;
                break;
            case WeaponType.Laser:
                currentWeapon = weapons[1];
                currentWeapon.CanMineResource = MiningResource.MiningResourceType.LaserResource;
                break;
            case WeaponType.RemoteDrill:
                currentWeapon = weapons[2];
                currentWeapon.CanMineResource = MiningResource.MiningResourceType.RemoteDrillResource;
                break;
        }
//		currentWeapon.gameObject.SetActive(true);
        currentWeapon.Initialize();
    }

    public void NextWeapon()
    {
        if (currentWeapon.CanMineResource == MiningResource.MiningResourceType.DrillResource)
        {
            ChangeWeapon(WeaponType.Laser);
        }
        else if (currentWeapon.CanMineResource == MiningResource.MiningResourceType.LaserResource)
        {
            ChangeWeapon(WeaponType.RemoteDrill);
        }
        else if (currentWeapon.CanMineResource == MiningResource.MiningResourceType.RemoteDrillResource)
        {
            ChangeWeapon(WeaponType.Drill);
        }
    }

    public void PreviousWeapon()
    {
        if (currentWeapon.CanMineResource == MiningResource.MiningResourceType.DrillResource)
        {
            ChangeWeapon(WeaponType.RemoteDrill);
        }
        else if (currentWeapon.CanMineResource == MiningResource.MiningResourceType.LaserResource)
        {
            ChangeWeapon(WeaponType.Drill);
        }
        else if (currentWeapon.CanMineResource == MiningResource.MiningResourceType.RemoteDrillResource)
        {
            ChangeWeapon(WeaponType.Laser);
        }
    }

    void Start()
    {
        currentWeapon.Initialize();
    }

}
