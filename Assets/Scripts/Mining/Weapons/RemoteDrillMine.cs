﻿using UnityEngine;
using System.Collections;

public class RemoteDrillMine : MiningResource
{

    public bool isPickable = false;
    DisplayElements displayElements;

    // Use this for initialization
    IEnumerator Start()
    {
        displayElements = GameObject.FindObjectOfType<DisplayElements>();
        yield return new WaitForSeconds(15f);
        audio.loop = true;
        audio.Play();
        isPickable = true;
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.transform.name == "Player" && isPickable)
        {
            displayElements.SetCargo(displayElements.Cargo + CargoLoad);
            Destroy(gameObject);
        }
    }
}
