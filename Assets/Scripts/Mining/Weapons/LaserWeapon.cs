﻿using UnityEngine;
using System.Collections;

public class LaserWeapon : Weapon
{
    public override void FireWeapon()
    {
        base.FireWeapon();

	}
	
	public override void MineResource(MiningResource resource)
	{
		base.MineResource(resource);
		GetComponent<Animator>().SetBool("Drill", true);
	}
	
	public override void OnMiningEnd()
	{
		GetComponent<Animator>().SetBool("Drill", false);
	}
}
