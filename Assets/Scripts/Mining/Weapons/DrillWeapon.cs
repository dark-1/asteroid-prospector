﻿using UnityEngine;
using System.Collections;

public class DrillWeapon : Weapon
{
	public ParticleSystem particles;

    public override void FireWeapon()
    {
        base.FireWeapon();
    }

	public override void MineResource(MiningResource resource)
	{
		particles.enableEmission = true;
		base.MineResource(resource);
		GetComponent<Animator>().SetBool("Drill", true);
	}

	public override void OnMiningEnd()
	{
		particles.enableEmission = false;
		GetComponent<Animator>().SetBool("Drill", false);
	}
}
