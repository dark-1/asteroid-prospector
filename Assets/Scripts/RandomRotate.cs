﻿using UnityEngine;
using System.Collections;

public class RandomRotate : MonoBehaviour {

	Vector3 rotation;

	// Use this for initialization
	void Start () {
		rotation = new Vector3(Random.value*.2f, Random.value*.2f, Random.value*.2f);
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(rotation);
	}
}
