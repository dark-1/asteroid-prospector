﻿using UnityEngine;
using System.Collections;

public class SunElement : MonoBehaviour
{

    private DisplayElements displayElements;
    public Transform sunTransform;
    public bool isIlluminated;
    TextMesh textMesh;

    public int secondsOnSunToEnergyUp = 10;
    int currentEnergy = 0;

    string sun = "☼";

    void Start()
    {
        textMesh = GetComponent<TextMesh>();
        InvokeRepeating("CheckSun", 1f, 1f);
        displayElements = GameObject.FindObjectOfType<DisplayElements>();
        displayElements.SetEnergy(5);
        textMesh.text = "";
    }

    void CheckSun()
    {
        var vehicle = GameObject.FindObjectOfType<PlayerController>().gameObject;
        RaycastHit rayHit;
        Debug.DrawRay(vehicle.transform.position, sunTransform.position - vehicle.transform.position, Color.red, 0.5f);

        if (!Physics.Raycast(vehicle.transform.position, sunTransform.position - vehicle.transform.position, out rayHit))
        {
            isIlluminated = true;
            textMesh.text = sun;
            currentEnergy++;
            if (currentEnergy == secondsOnSunToEnergyUp)
            {
                currentEnergy = 0;
                ChargeEnergy();
            }
        }
        else
        {
            isIlluminated = false;
            textMesh.text = "";
        }

    }

    private void ChargeEnergy()
    {
        displayElements.SetEnergy(++displayElements.Energy);
    }



}
