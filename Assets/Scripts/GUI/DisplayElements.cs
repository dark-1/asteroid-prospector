﻿using UnityEngine;
using System.Collections;

public class DisplayElements : MonoBehaviour
{
    //☼

    public int MaxHealth = 15;
    public int MaxCargo = 15;
    public int MaxActionLines = 15;
    public int MaxEnergy = 15;

    public int Health;
    public int Cargo;
    public int Energy;

    private TextMesh warningMesh;
    private TextMesh healthMesh;
    private TextMesh cargoMesh;
    private TextMesh actionMesh;
    private TextMesh weaponMesh;
    private TextMesh energyMesh;
    private TextMesh notificationMesh;

    private PlayerWeaponController playerWeaponController;

    public int Metal;
    public int Crystal;
    public int Gas;

    public enum CargoType
    {
        Metal,
        Crystal,
        Gas
    }

    void Awake()
    {
        cargoMesh = transform.FindChild("Cargo").GetComponent<TextMesh>();
        healthMesh = transform.FindChild("Health").GetComponent<TextMesh>();
        actionMesh = transform.FindChild("Action").GetComponent<TextMesh>();
        warningMesh = transform.FindChild("Warning").GetComponent<TextMesh>();
        weaponMesh = transform.FindChild("Weapon").GetComponent<TextMesh>();
        energyMesh = transform.FindChild("Energy").GetComponent<TextMesh>();
        notificationMesh = transform.FindChild("Notification").GetComponent<TextMesh>();

        playerWeaponController = GameObject.FindObjectOfType<PlayerWeaponController>();
        actionMesh.text = "";
        notificationMesh.text = "";
    }


    public bool SetCargo(int cargo)
    {
        if (cargo > MaxCargo)
        {
            StartWarning("CARGO OVERLOAD");
            return false;
        }

        //TODO change color
        Cargo = cargo;
        cargoMesh.text = "CARGO [" + new string('|', cargo) + new string(' ', MaxCargo - cargo) + "]";
        StartCoroutine(BlinkDisplay(cargoMesh, false));
        return true;
    }

    public bool SetCargo(int cargo, CargoType type)
    {
        if (cargo > MaxCargo)
        {
            StartWarning("CARGO OVERLOAD");
            return false;
        }

        switch (type)
        {
            case CargoType.Crystal:
                Crystal += Cargo - cargo;
                break;
            case CargoType.Gas:
                Gas += Cargo - cargo;
                break;
            case CargoType.Metal:
                Metal += Cargo - cargo;
                break;
        }

        //TODO change color
        Cargo = cargo;
        cargoMesh.text = "CARGO [" + new string('|', cargo) + new string(' ', MaxCargo - cargo) + "]";
        StartCoroutine(BlinkDisplay(cargoMesh, false));
        return true;
    }

    public void ShowNotification(string message)
    {
        notificationMesh.text = message;
        StartCoroutine(CoStartBlinking(message, notificationMesh));
    }

    public bool SetHealth(int health)
    {
        Health = health;

        if (Health < 0)
            return false;

        Health = Mathf.Clamp(Health, 0, MaxHealth);

        healthMesh.text = "HEALTH [" + new string('|', Health) + new string(' ', MaxHealth - Health) + "]";

        StartCoroutine(BlinkDisplay(healthMesh, false));
        return true;
    }

    public void SetEnergy(int energy)
    {
        Energy = energy;
        Energy = Mathf.Clamp(Energy, 0, MaxEnergy);

        energyMesh.text = "ENERGY [" + new string('|', Energy) + new string(' ', MaxEnergy - Energy) + "] ";
        StartCoroutine(BlinkDisplay(energyMesh, false));
    }

    private IEnumerator BlinkDisplay(TextMesh displayElement, bool clearOnEnd)
    {
        var text = displayElement.text;
        displayElement.text = "";
        yield return new WaitForSeconds(0.2f);
        displayElement.text = text;
        if (clearOnEnd)
            displayElement.text = "";
    }


    public void StartAction(string action, float time)
    {
        StartCoroutine(CoStartAction(action, time));
        //TODO block input
    }

    public void SetWeapon(string type)
    {
        weaponMesh.text = "< " + type + " >";
    }

    public IEnumerator CoStartAction(string action, float time)
    {
        playerWeaponController.IsControlEnabled = false;
        float period = time / MaxActionLines;
        int currentAction = 0;
        while (currentAction <= MaxActionLines)
        {
            actionMesh.text = action + "\n" + "[" + new string('|', currentAction) + new string(' ', MaxActionLines - currentAction) + "]";
            currentAction++;
            yield return new WaitForSeconds(period);
        }
        actionMesh.text = "";
        playerWeaponController.IsControlEnabled = true;
    }

    public void StartWarning(string message)
    {
        StartCoroutine(CoStartBlinking(message, warningMesh));
    }

    public IEnumerator CoStartBlinking(string message, TextMesh resource)
    {
        playerWeaponController.IsControlEnabled = false;
        for (int i = 0; i < 3; i++)
        {
            resource.text = message;
            yield return new WaitForSeconds(0.5f);
            resource.text = "";
            yield return new WaitForSeconds(0.5f);
        }

        resource.text = "";
        playerWeaponController.IsControlEnabled = true;
    }

    void Start()
    {
        warningMesh.text = "";
        SetHealth(MaxHealth);
        SetCargo(0);
    }

}
