﻿using UnityEngine;
using System.Collections;

public class PlayerHUD : MonoBehaviour {

	public Color defaultColor;
	public Color hoverColor;
	public Texture crosshairTex;
	public bool asteroidHover = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI () {
		if(asteroidHover){
			GUI.color = hoverColor;
		}
		else{
			GUI.color = defaultColor;
		}
		GUI.DrawTexture(new Rect((Screen.width - crosshairTex.width)*.5f, (Screen.height - crosshairTex.height)*.5f, crosshairTex.width, crosshairTex.height), crosshairTex);
	}
}
