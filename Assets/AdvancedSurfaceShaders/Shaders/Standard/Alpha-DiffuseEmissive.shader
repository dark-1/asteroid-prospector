Shader "Advanced SS/Standard/Transparent/Diffuse Emissive"
{
    Properties
    {
        _Color ("Main Color", Color) = (1,1,1,1)
        _EmissiveStrength ("Emissive Strength", Float) = 1.0
        _MainTex ("Texture", 2D) = "white" {}
        _EmissiveMap ("EmissiveMap (RGB)", 2D) = "black" {}
    }

    SubShader
    {
        Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
        LOD 500

        CGPROGRAM

        #define ADVANCEDSS_EMISSIVE

        #if defined(SHADER_API_D3D11) || defined(SHADER_API_D3D11_9X)
        #pragma target 5.0
        #else
        #pragma target 3.0
        #endif
        #include "../AdvancedSS.cginc"
        #pragma surface advancedSurfaceShader Lambert alpha

        ENDCG
    }

    Fallback "Diffuse"
}